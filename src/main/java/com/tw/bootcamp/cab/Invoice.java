package com.tw.bootcamp.cab;

import java.util.Objects;

public class Invoice {

    private int totalFare;
    private int totalRides;
    private double averageFare;

    public Invoice(int totalFare, int totalRides, double averageFare) {
        this.totalFare = totalFare;
        this.totalRides = totalRides;
        this.averageFare = averageFare;
    }

    public int getTotalFare() {
        return totalFare;
    }

    public int getTotalRides() {
        return totalRides;
    }

    public double getAverageFare() {
        return averageFare;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return totalFare == invoice.totalFare &&
                totalRides == invoice.totalRides &&
                averageFare == invoice.averageFare;
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalFare, totalRides, averageFare);
    }
}
