package com.tw.bootcamp.cab;

import static java.lang.Math.max;

public class Ride {

    private static final int COST_PER_KM = 10;
    private static final int WAITING_TIME_IN_MIN = 2;
    private static final int MINIMUM_FARE = 40;

    private final int distanceCoveredInKm;
    private final int waitingTimeInMin;

    public Ride(int distanceCoveredInKm, int waitingTimeInMin) {
        this.distanceCoveredInKm = distanceCoveredInKm;
        this.waitingTimeInMin = waitingTimeInMin;
    }

    public int calculateFare() {
        return max(MINIMUM_FARE, this.distanceCoveredInKm * COST_PER_KM + this.waitingTimeInMin * WAITING_TIME_IN_MIN);
    }


}
