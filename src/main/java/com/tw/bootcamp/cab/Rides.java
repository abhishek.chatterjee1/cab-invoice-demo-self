package com.tw.bootcamp.cab;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;


public class Rides {

    private final List<Ride> takenRides = new ArrayList<>();


    public int add(Ride... rides) {

        takenRides.addAll(asList(rides));
        int totalFare = asList(rides)
                .stream()
                .mapToInt(ride -> ride.calculateFare())
                .sum();

        return totalFare;
    }

    public Optional<Invoice> generateInvoice() {
        if (this.takenRides.isEmpty())
            return Optional.empty();

        int totalFare = this.takenRides
                .stream()
                .mapToInt(ride -> ride.calculateFare())
                .sum();

        double avgFare = new Double(totalFare) / new Double(this.takenRides.size());
        return Optional.of(new Invoice(totalFare, this.takenRides.size(), avgFare));
    }
}
