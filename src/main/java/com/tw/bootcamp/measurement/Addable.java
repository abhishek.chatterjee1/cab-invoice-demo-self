package com.tw.bootcamp.measurement;

@FunctionalInterface
public interface Addable {
    Measurement add(Measurement measurement2) throws UnsupportedOperationException;
}
