package com.tw.bootcamp.measurement;

import java.util.Objects;

public abstract class Measurable {

    final Double magnitude;
    final Unit unit;

    public Measurable(Double magnitude, Unit unit) {
        this.magnitude = magnitude;
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Measurable measurable = (Measurable) o;
        return this.isCompatible(measurable) && this.magnitude.equals(measurable.convertTo(this.unit));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.magnitude, this.unit);
    }

    @Override
    public String toString() {
        return magnitude + unit.toString();
    }

    public boolean isCompatible(Measurable measurable) {
        return this.unit.isCompatible(measurable.unit);
    }

    public Double convertTo(Unit unit) throws UnsupportedOperationException {
        return this.unit.convert(this.magnitude, unit);
    }
}
