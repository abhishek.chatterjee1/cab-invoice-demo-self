package com.tw.bootcamp.measurement;


public class Measurement extends Measurable implements Addable {

    /**
     * user can create temperature as Measurement instance and add, but will somebody do
     * Measurement m = new Measurement(30, CELSIUS) when there is a Temperature class already in the library??
     * if so, solution is, make Measurement abstract and derive Length, Weight class as child of Measurement
     * @param magnitude
     * @param unit
     */
    public Measurement(Double magnitude, Unit unit) {
        super(magnitude, unit);
    }

    @Override
    public Measurement add(Measurement measurement) throws UnsupportedOperationException {
        if (!this.isCompatible(measurement))
            throw new UnsupportedOperationException("Incompatible argument");
        return new Measurement(this.magnitude + measurement.convertTo(this.unit), this.unit);
    }

}
