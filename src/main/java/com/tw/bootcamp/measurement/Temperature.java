package com.tw.bootcamp.measurement;

public class Temperature extends Measurable {

    public Temperature(Double magnitude, Unit unit) {
        super(magnitude, unit);
    }
}
