package com.tw.bootcamp.measurement;

import java.util.function.Function;

public enum Unit {


    KILOMETER(val -> val * 1000 * 100, val -> val / (1000 * 100), UnitType.LENGTH),
    METER(val -> val * 100, val -> val / 100, UnitType.LENGTH),
    CENTIMETER(Function.identity(), Function.identity(), UnitType.LENGTH), //reference

    KILOGRAM(val -> val * 1000 * 1000, val -> val / (1000 * 1000), UnitType.WEIGHT),
    GRAM(val -> val * 1000, val -> val / 1000, UnitType.WEIGHT),
    MILLIGRAM(Function.identity(), Function.identity(), UnitType.WEIGHT),  //reference

    KELVIN(val -> val - 273.15, val -> val + 273.15, UnitType.TEMPERATURE),
    FAHRENHEIT(val -> ((val - 32) * 5) / 9, val -> ((val * 9) / 5) + 32, UnitType.TEMPERATURE),
    CELSIUS(Function.identity(), Function.identity(), UnitType.TEMPERATURE);   //reference


    enum UnitType {
        LENGTH, WEIGHT, TEMPERATURE;
    }

    //Function to convert this unit to reference unit
    private Function<Double, Double> converterToBase;
    private Function<Double, Double> converterFromBase;
    private UnitType type;

    Unit(Function<Double, Double> converterToBase, Function<Double, Double> converterFromBase, UnitType type) {
        this.converterToBase = converterToBase;
        this.converterFromBase = converterFromBase;
        this.type = type;
    }

    public boolean isCompatible(Unit unit) { return this.type == unit.type; }

    public Double convert(Double val, Unit to) {
        if (!this.isCompatible(to))
            throw new UnsupportedOperationException("Could not convert " + this + " to " + to);

        Double valueInBaseUnit = this.converterToBase.apply(val);
        return to.converterFromBase.apply(valueInBaseUnit);

    }

}


