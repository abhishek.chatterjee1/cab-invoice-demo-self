package com.tw.bootcamp.shapes;

public class Rectangle implements Shape {

    private static final int PERIMETER_CALCULATION_FACTOR = 2;

    private final int length;
    private final int width;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public Long area() {
        return (long) (this.length * this.width);
    }

    @Override
    public Long perimeter() {
        return (long) (PERIMETER_CALCULATION_FACTOR * (this.width + this.length));
    }

    public static Rectangle square(int length) {
        return new Rectangle(length, length);
    }
}
