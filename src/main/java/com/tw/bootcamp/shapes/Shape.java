package com.tw.bootcamp.shapes;

public interface Shape {

    Long area();
    Long perimeter();

}
