package com.tw.bootcamp.cab;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RidesTest {

    public static final int MINIMUM_FARE = 40;

    @Test
    void shouldBeFareRsFourtyWhenDistanceIsOneKilometer() {
        Rides rides = new Rides();

        int oneKmFare = rides.add(new Ride(1, 0));

        assertEquals(MINIMUM_FARE, oneKmFare);
    }

    @Test
    void shouldBeFareRsFourtyWhenDistanceIsZeroAndWaitingTimeIsOneMin() {
        Rides rides = new Rides();

        int twoKmFare = rides.add(new Ride(0, 1));

        assertEquals(MINIMUM_FARE, twoKmFare);
    }

    @Test
    void shouldBeFareRsFourtyWhenWaitingTimeIsOneMinuteAndDistanceIsOneKilometer() {
        Rides rides = new Rides();

        int oneMinuteFare = rides.add(new Ride(1, 1));

        assertEquals(MINIMUM_FARE, oneMinuteFare);
    }


    @Test
    void shouldBeExactFareWhenTotalFareIsMoreThanMinimumFare() {
        Rides rides = new Rides();

        int minimumFare = rides.add(new Ride(12, 11));

        assertEquals(142, minimumFare);
    }

    @Test
    void shouldBeExactFareWhenTotalFareIsEqualToMinimumFare() {
        Rides rides = new Rides();

        int minimumFare = rides.add(new Ride(4, 0));

        assertEquals(MINIMUM_FARE, minimumFare);
    }

    @Test
    void shouldAggregateToTalFareWhenMultipleRidesTaken() {
        Rides rides = new Rides();

        rides.add(new Ride(1, 0), new Ride(10, 5));

        assertEquals(150, rides.generateInvoice().get().getTotalFare());
    }

    @Test
    void shouldGenerateInvoice() {
        Rides rides = new Rides();
        rides.add(new Ride(1, 0), new Ride(10, 5));
        Optional<Invoice> oInvoice = rides.generateInvoice();
        assertEquals(true, oInvoice.isPresent());
        assertEquals(new Invoice(150, 2, 75), oInvoice.get());
    }

    @Test
    void shouldNotGenerateInvoiceWhenNoRidesTaken() {
        Rides rides = new Rides();
        assertEquals(Optional.empty(), rides.generateInvoice());
    }

}
