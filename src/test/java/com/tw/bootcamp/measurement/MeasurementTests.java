package com.tw.bootcamp.measurement;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MeasurementTests {

    @Test
    void thousandMtrShouldBeEqualToThousandMtr() {
        Measurement expected = new Measurement(1000.0, Unit.METER);
        Measurement actual = new Measurement(1000.0, Unit.METER);
        assertEquals(expected, actual);

    }

    @Test
    void oneMtrShouldNotBeEqualToTwoMtr() {
        assertNotEquals(new Measurement(1.0, Unit.METER), new Measurement(2.0, Unit.METER));
    }

    @Test
    void thousandMeterShouldBeEqualToOneKm() {
        assertEquals(new Measurement(1000.0, Unit.METER), new Measurement(1.0, Unit.KILOMETER));
    }

    @Test
    void hundredCmShouldBeEqualToOneM() {
        assertEquals(new Measurement(100.0, Unit.CENTIMETER), new Measurement(1.0, Unit.METER));
    }


    @Test
    void thousandMtrAddingToThousandMtrShouldBeTwoThousandMtr() throws UnsupportedOperationException {
        assertEquals(new Measurement(2000.0, Unit.METER),
                new Measurement(1000.0, Unit.METER).add(new Measurement(1000.0, Unit.METER)));
    }

    @Test
    void oneKmAddingToThreeHundredMtrShouldBeOnePointThreeKm() throws UnsupportedOperationException {
        assertEquals(new Measurement(1.3, Unit.KILOMETER),
                new Measurement(1.0, Unit.KILOMETER).add(new Measurement(300.0, Unit.METER)));
    }

    @Test
    void fiveHundredMtrAddingToOneKmShouldBeOneThousandFiveHundredMtr() throws UnsupportedOperationException {
        assertEquals(new Measurement(1500.0, Unit.METER),
                new Measurement(500.0, Unit.METER).add(new Measurement(1.0, Unit.KILOMETER)));
    }

    @Test
    void oneGMShouldBeEqualToOneGM() {
        Measurement expected = new Measurement(1.0, Unit.GRAM);
        Measurement actual = new Measurement(1.0, Unit.GRAM);
        assertEquals(expected, actual);
    }

    @Test
    void oneCMShouldNotEqualToOneMG() {
        assertNotEquals(new Measurement(1.0, Unit.CENTIMETER), new Measurement(1.0, Unit.MILLIGRAM));
    }

    @Test
    void shouldNotAddLengthWithWeight() {
        assertThrows(UnsupportedOperationException.class, () -> new Measurement(1.0, Unit.METER).add(new Measurement(1.0, Unit.GRAM)));
    }

    @Test
    void zeroCelsiusShouldEqualToTwoSeventyThreePointOneFiveKelvin() {
        assertEquals(new Temperature(0.0, Unit.CELSIUS), new Temperature(273.15, Unit.KELVIN));
    }

    @Test
    void minusFortyCelsiusShouldEqualMinusFortyFahrenheit() {
        assertEquals(new Temperature(-40.0, Unit.CELSIUS), new Temperature(-40.0, Unit.FAHRENHEIT));
    }




}
