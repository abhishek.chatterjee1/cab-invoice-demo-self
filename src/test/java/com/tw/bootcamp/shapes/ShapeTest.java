package com.tw.bootcamp.shapes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapeTest {

    @Test
    void shouldCalculateExactAreaOfRectangleWithZeroSizeEdges() {
        Shape shape = new Rectangle(0,0);
        assertEquals(Long.valueOf(0),shape.area());
    }

    @Test
    void shouldCalculateExactAreaOfRectangleWithNonZeroSizeEdges() {
        Shape shape = new Rectangle(5,4);
        assertEquals(new Long(20),shape.area());
    }

    @Test
    void shouldReturnPerimeterOfRectangleWithZeroSizeEdges() {
        Shape shape = new Rectangle(0,0);
        assertEquals(Long.valueOf(0),shape.perimeter());
    }

    @Test
    void shouldReturnPerimeterOfRectangleWithNonZeroSizeEdges() {
        Shape shape = new Rectangle(5,4);
        assertEquals(Long.valueOf(18),shape.perimeter());
    }

    @Test
    void shouldCalculateExactAreaOfSquareWithZeroSizeEdges() {
        Shape shape = Rectangle.square(0);
        assertEquals(Long.valueOf(0),shape.area());
    }

    @Test
    void shouldCalculateExactAreaOfSquareWithNonZeroSizeEdges() {
        Shape shape = Rectangle.square(5);
        assertEquals(new Long(25),shape.area());
    }

    @Test
    void shouldReturnPerimeterOfSqaureWithZeroSizeEdges() {
        Shape shape = Rectangle.square(0);
        assertEquals(Long.valueOf(0),shape.perimeter());
    }

    @Test
    void shouldReturnPerimeterOfSquareWithNonZeroSizeEdges() {
        Shape shape = Rectangle.square(5);
        assertEquals(Long.valueOf(20),shape.perimeter());
    }


}
